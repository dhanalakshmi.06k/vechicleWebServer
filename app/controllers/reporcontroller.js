var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ccTransactionModel = mongoose.model('companyTranasactionDetails');

module.exports = function (app){
    app.use('/', router);
};

router.get('/transactionReportByDay/', function(req, res, next) {

    var currentDate =new Date(req.query.currentDate);
    console.log("-----------currentDate---")
    console.log(currentDate)


    ccTransactionModel.find({"updated": currentDate.toISOString()}, { _id: 0, __v: 0}
        ,function(err,result){
            if(err){
                res.send(err)
                console.log(err.stack)
            }else{
                res.send(result)
            }

        })
})

router.get('/transactionReportByDateRange/', function(req, res, next) {

    var currentDate =new Date(req.query.startDate);
    console.log("-----------currentDate---")
        ccTransactionModel.find({$and :
                    [
                        {$and: [{"updated": { "$gte" :  new Date(req.query.startDate).toISOString()} } , {"updated": { "$lte" : new Date(req.query.endDate).toISOString()} }]} ,

                    ]

            }, { _id: 0, __v: 0}
            ,function(err,result){
                if(err)
                {
                    console.log(err);
                }
                else
                {
                    console.log(result);
                    res.send(result);

                }
            })
})

router.get('/transactionReportByCompanyNameAndCCType/', function(req, res, next) {
    let cctype = req.query.cctype;
    console.log("-----------companyName---")
    ccTransactionModel.find(
        {$and: [{"companyTransactionSelected": req.query.companyName.replace(/'%20'/g, ' ') } , { "corporateContract" :cctype.replace(/'%20'/g, ' ') }]}
        , { _id: 0, __v: 0},function(err,result){
            if(err){
                next()
                console.log(err.stack)
            }else{
                res.send(result)
            }

        })
})


router.get('/transactionReportByCompanyName/', function(req, res, next) {
    let cctype = req.query.cctype;
    console.log("-----------companyName---")
    ccTransactionModel.find({"companyTransactionSelected": req.query.companyName.replace(/'%20'/g, ' ') }
        , { _id: 0, __v: 0},function(err,result){
            if(err){
                next()
                console.log(err.stack)
            }else{
                res.send(result)
            }

        })
})



router.get('/transactionReportOnlyByCCType/', function(req, res, next) {
    let cctype = req.query.ccType;
    console.log("-----------companyName---")
    ccTransactionModel.find({"corporateContract": req.query.ccType.replace(/'%20'/g, ' ') }
        , { _id: 0, __v: 0},function(err,result){
            if(err){
                next()
                console.log(err.stack)
            }else{
                res.send(result)
            }

        })
})




router.get('/transactionReportByCompanyNameAndVehicleType/', function(req, res, next) {
    let cctype = req.query.cctype;
    console.log("-----------companyName---")
    ccTransactionModel.find(
        {$and: [{"companyTransactionSelected": req.query.companyName.replace(/'%20'/g, ' ') } , { "vehicleType" :vehicleType.replace(/'%20'/g, ' ') }]}
        ,function(err,result){
            if(err){
                res.send(err)
                console.log(err.stack)
            }else{
                res.send(result)
            }

        })
})

router.get('/transactionReportByCompanyNameAndVehicleTypeAndccType/', function(req, res, next) {
    let cctype = req.query.ccType;
    let vehicleType = req.query.vehicleType;
    console.log(cctype)
    console.log(vehicleType)
    console.log(req.query.companyName)
    ccTransactionModel.find(
        {$and: [{"companyTransactionSelected": req.query.companyName.replace(/'%20'/g, ' ') } , { "vehicleType" :vehicleType.replace(/'%20'/g, ' ') },
                {"companyTransactionSelected": req.query.companyName.replace(/'%20'/g, ' ') } , { "corporateContract" :cctype.replace(/'%20'/g, ' ') }]}
        ,function(err,result){
            if(err){
                res.send(err)
                console.log(err.stack)
            }else{
                res.send(result)
            }

        })
})


router.get('/transactionReport/', function(req, res, next) {

    let cctype = req.query.cctype;
    let companyName = req.query.companyName;
    let vehicleType = req.query.vehicleType;


    console.log(req.query.cctype)
    console.log(companyName)


    if(companyName!=undefined && cctype!=undefined){
        ccTransactionModel.find(
            {$and: [{"companyTransactionSelected": req.query.companyName.replace(/'%20'/g, ' ') } , { "corporateContract" :cctype }]}
            ,function(err,result){
                if(err){
                    next()
                    console.log(err.stack)
                }else{
                    res.send(result)
                }

            })
    }

    if(cctype!=undefined){
        console.log("99999startDate999endDate99990"+req.query.cctype)

        ccTransactionModel.find({"corporateContract" :req.query.cctype.replace(/'%20'/g, ' ')},function(err,result){
            if(err){
                // res.send(err)
                next()
                console.log(err.stack)
            }else{
                res.send(result)
            }

        })
    }
    if(companyName!=undefined){
        ccTransactionModel.find({"companyTransactionSelected" :req.query.companyName.replace(/'%20'/g, ' ')},function(err,result){
            if(err){
                next()
                // res.send(err)
                console.log(err.stack)
            }else{
                res.send(result)
            }

        })
    }


    if(companyName && vehicleType && cctype== undefined){
        ccTransactionModel.find(
            {$and: [{"companyTransactionSelected": req.query.companyName.replace(/'%20'/g, ' ') } , { "vehicleType" :vehicleType.replace(/'%20'/g, ' ') }]}
            ,function(err,result){
                if(err){
                    res.send(err)
                    console.log(err.stack)
                }else{
                    res.send(result)
                }

            })
    }







})