var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose');
var applicationDataAccess = require("../dao").applicationParameter;
var applicationConfigDao = require('../dao/applicaitionParameterConfig');
var _ = require("lodash");
let applicationConfigDAO = require("../dao").applicationParameter;

module.exports = function (app){
    app.use('/', router);
};


router.get('/applicationParameterConfig', function(req, res, next) {
    let methodToBeExec =null;
    if(req.query.parameterType){
        methodToBeExec = applicationConfigDAO.getByType(req.query.parameterType);
    }else{
        methodToBeExec = applicationConfigDAO.getAll();
    }
    methodToBeExec.then(function(allAssets){
        res.send(allAssets);
    })
});