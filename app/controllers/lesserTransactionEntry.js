var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    lesserTransactionDetailsModel = mongoose.model('lesserTransactionDeatils');

module.exports = function (app){
    app.use('/', router);
};


router.post('/lesserTransaction', function(req, res, next) {

    var lesserTransactionDetails = new lesserTransactionDetailsModel(req.body);
    lesserTransactionDetails.save(function(err, result) {
        if (err){
            console.log('fuelDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/lesserTransaction/count', function(req, res, next) {
    lesserTransactionDetailsModel.count(function(err,author){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: author};
        res.send(count);
    });
})

router.get('/lesserTransactionByRange/:start/:range', function(req, res, next) {
    lesserTransactionDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/lesserTransaction/:lesserTransactionMongoId',function(req,res,next){
    lesserTransactionDetailsModel.findOne({"_id":req.params.lesserTransactionMongoId},function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

});



router.post('/lesserTransaction/:lesserTransactionMongoId', function(req, res, next) {

    lesserTransactionDetailsModel.findOneAndUpdate({"_id":req.params.lesserTransactionMongoId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})

router.get('/lesserTransaction/:ccType', function(req, res, next) {

    lesserTransactionDetailsModel.find({"corporateContract" :req.params.ccType.replace(/'%20'/g, ' ')},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})

router.delete('/lesserTransaction/:lesserAssociationMongoId',function(req, res, next){

    lesserTransactionDetailsModel.remove({"_id":req.params.lesserAssociationMongoId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});


/*
router.get('/lesserAssociationByLesserNameAndCCType/:lesserName/:ccType',function(req,res,next){
    console.log(req.params.lesserName)
    lesserAssociationModel.find({$and :
            [
                {$and:
                        [{"lesserSelectedForDropDown":req.params.lesserName} ,
                            {"corporateContract": req.params.ccType}]} ,

            ]

    },function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })
});
*/

