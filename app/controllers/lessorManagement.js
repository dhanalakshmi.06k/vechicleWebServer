var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    LessorManagementModel = mongoose.model('lessorManagementDetails');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/lessorManagementDetails', function(req, res, next) {

    var lessorManagementModel = new LessorManagementModel(req.body);
    lessorManagementModel.save(function(err, result) {
        if (err){
            console.log('lessorManagementDetails failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/lessorManagementDetails/count', function(req, res, next) {
    LessorManagementModel.count(function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})

router.get('/allLessorManagementDetails', function(req, res, next) {
    LessorManagementModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/lessorManagementBymongoId/:lessorManagementMongoId',function(req,res,next){

        LessorManagementModel.findOne({"_id":req.params.lessorManagementMongoId},function(err,result){
                      if(err)
                      {
                     console.log(err);
                      }
                      else
                      {
                         console.log(result);
                         res.send(result);

                      }
                      })

});



router.post('/editLessorManagementBymongoId', function(req, res, next) {

        LessorManagementModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},
        function(err,result)
            {
                if(err){
                    console.log(err.stack)
                }
                else{
                    res.send(result)
                }

            });

})


router.delete('/lessorManagementBymongoId/:lessorManagementMongoId',function(req, res, next){

            LessorManagementModel.remove({"_id":req.params.lessorManagementMongoId},function(err,result)
            {
            if(err)
            {
            console.log(err);
            }
            else
            {
            res.send(result)
            }

            });
            });




