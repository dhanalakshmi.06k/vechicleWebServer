var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ShiftModel = mongoose.model('shiftType');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/shiftDetails', function(req, res, next) {
console.log('abcddsaf',req.body)
    var shiftModel = new ShiftModel(req.body);
    shiftModel.save(function(err, result) {
        if (err){
            console.log('lessorDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});


router.get('/shiftType/count', function(req, res, next) {
    ShiftModel.count(function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})


router.get('/allShiftDetailDropDown', function(req, res, next) {
    ShiftModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})




router.get('/allShiftDetails/:start/:range', function(req, res, next) {
    ShiftModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/shiftBymongoId/:shiftMongoId',function(req,res,next){

        ShiftModel.findOne({"_id":req.params.shiftMongoId},function(err,result){
                      if(err)
                      {
                     console.log(err);
                      }
                      else
                      {
                         console.log(result);
                         res.send(result);

                      }
                      })

});



router.post('/editShiftBymongoId/:shiftId', function(req, res, next) {

        ShiftModel.findOneAndUpdate({"_id":req.params.shiftId},req.body,{upsert: true, new: true},
        function(err,result)
            {
                if(err){
                    console.log(err.stack)
                }
                else{
                    res.send(result)
                }

            });

})


router.delete('/shiftBymongoId/:shiftMongoId',function(req, res, next){

            ShiftModel.remove({"_id":req.params.shiftMongoId},function(err,result)
            {
            if(err)
            {
            console.log(err);
            }
            else
            {
            res.send(result)
            }

            });
            });




