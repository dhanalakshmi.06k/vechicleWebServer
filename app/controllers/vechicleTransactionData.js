var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    VechicleTransactionDetailsModel = mongoose.model('vechicleTransactionDetails');

module.exports = function (app){
    app.use('/', router);
};


router.post('/saveDailyVechicleTransaction', function(req, res, next) {


    var vechicleTransactionDetailsModel = new VechicleTransactionDetailsModel(req.body);
    vechicleTransactionDetailsModel.save(function(err, result) {
        if (err){
            console.log('fuelDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/allVechicleTransaction/count', function(req, res, next) {


    VechicleTransactionDetailsModel.count(function(err,author){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: author};
        res.send(count);
    });
})

router.get('/allVechicleTransaction/:start/:range', function(req, res, next) {
    VechicleTransactionDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


/*router.get('/allVechicleTransactionDropDown', function(req, res, next) {
    VechicleTransactionDetailsModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})*/


router.get('/VechicleTransactionBymongoId/:mongoId',function(req,res,next){
    console.log('fuelMongoId', req.params.fuelMongoId);
    VechicleTransactionDetailsModel.findOne({"_id":req.params.mongoId},function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

});



router.post('/VechicleTransactionlDetails/:mongoDbId', function(req, res, next) {

    VechicleTransactionDetailsModel.findOneAndUpdate({"_id":req.params.mongoDbId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})


router.delete('/VechicleTransactionBymongoId/:mongoId',function(req, res, next){

    VechicleTransactionDetailsModel.remove({"_id":req.params.mongoId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});




