var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    FuelExpenseModel = mongoose.model('fuelExpenseType');

    module.exports = function (app){
        app.use('/', router);
    };


router.post('/fuelExpenseDetails', function(req, res, next) {
console.log('abcddsaf',req.body)
    var fuelExpenseModel = new FuelExpenseModel(req.body);
    fuelExpenseModel.save(function(err, result) {
        if (err){
            console.log('fuelExpenseType failed: ' + err);
        }
        res.send(result);
    });
});


router.get('/allFuelExpenseDetails/:start/:range', function(req, res, next) {
    FuelExpenseModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})



router.get('/allFuelExpenseDetails/count', function(req, res, next) {
    FuelExpenseModel.count(function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})

router.get('/fuelExpenseBymongoId/:fuelExpenseMongoId',function(req,res,next){

        FuelExpenseModel.findOne({"_id":req.params.fuelExpenseMongoId},function(err,result){
                      if(err)
                      {
                     console.log(err);
                      }
                      else
                      {
                         console.log(result);
                         res.send(result);

                      }
                      })

});



router.post('/editFuelExpenseBymongoId', function(req, res, next) {

        FuelExpenseModel.findOneAndUpdate({"_id":req.body._id},req.body,{upsert: true, new: true},
        function(err,result)
            {
                if(err){
                    console.log(err.stack)
                }
                else{
                    res.send(result)
                }

            });

})


router.delete('/fuelExpenseBymongoId/:fuelExpenseMongoId',function(req, res, next){

            FuelExpenseModel.remove({"_id":req.params.fuelExpenseMongoId},function(err,result)
            {
            if(err)
            {
            console.log(err);
            }
            else
            {
            res.send(result)
            }

            });
            });




