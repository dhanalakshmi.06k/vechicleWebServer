var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    companyCorporateContractModel = mongoose.model('companyTranasactionDetails');

module.exports = function (app){
    app.use('/', router);
};




router.post('/getReportDetails', function(req, res, next) {
    console.log("**********************")
    console.log(req.body.filter)
    let filterObjArr=[]


    if(req.body.filter && req.body.filter.corporateContract){
    let ccType=req.body.filter.corporateContract.replace(/'%20'/g, ' ')
    var corporateContract = '{"corporateContract": "'+ ccType +'"}'
    filterObjArr.push( JSON.parse(corporateContract))
    }

     if(req.body.filter && req.body.filter.companyName){
        let cName=req.body.filter.companyName.replace(/'%20'/g, ' ')
        var compNameObj = '{"companyTransactionSelected": "'+ cName +'"}'
        filterObjArr.push( JSON.parse(compNameObj))
        }
        if(req.body.filter && req.body.filter.vehicleType){
                let vehicleType=req.body.filter.vehicleType.replace(/'%20'/g, ' ')
                var vehicleTypeObj = '{"vehicleType": "'+ vehicleType +'"}'
                filterObjArr.push( JSON.parse(vehicleTypeObj))
                }

         if(req.body.filter && req.body.filter.startDate){
                        var startDateObj = '{"updated": { "$gte" : "'+ startDate +'"}}'
                        filterObjArr.push( JSON.parse(startDateObj))
                        }

         if(req.body.filter && req.body.filter.endDate){
                        var endDateObj = '{"updated": { "$lte" : "'+ endDate +'"}}'
                        filterObjArr.push( JSON.parse(endDateObj))
                        }

            if(req.body.filter && req.body.filter.vehicleNumber){
                                let vehicleNumber=req.body.filter.vehicleNumber
                                               var vehicleNumberObj = '{"vehicleNumber": "'+ vehicleNumber +'"}'
                                               filterObjArr.push( JSON.parse(vehicleNumberObj))
                                 }

                    console.log("*************filterObjArr")
                        console.log(filterObjArr)

             companyCorporateContractModel.aggregate([
                    {
                        $project: {
                            _id: 0,
                            __v:0
                        }
                    },
                    {
                        $match: {
                            $and: filterObjArr
                        }
                    }
                ], function (err, result) {
                    if (err) {
                        console.log(err.stack)
                        res.send(err)

                    } else {
                        res.send(result)
                    }

                })



})

function extend(obj, src) {
    for (var key in src) {
        if (src.hasOwnProperty(key)) obj[key] = src[key];
    }
    return obj;
}

