var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    ccTransactionModel = mongoose.model('companyTranasactionDetails');

module.exports = function (app){
    app.use('/', router);
};


router.post('/CCTransaction', function(req, res, next) {
    var ccTransactionDetailsModel = new ccTransactionModel(req.body);
    ccTransactionDetailsModel.save(function(err, result) {
        if (err){
            console.log('fuelDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/CCTransaction/count', function(req, res, next) {
    ccTransactionModel.count(function(err,author){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: author};
        res.send(count);
    });
})

router.get('/CCTypeTransaction/:ccType', function(req, res, next) {
    ccTransactionModel.find({"corporateContract" :req.params.ccType.replace(/'%20'/g, ' ')},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})


router.get('/CCTypeTransactionByLmit/:ccType/:start/:range', function(req, res, next) {
    ccTransactionModel.find({"corporateContract" :req.params.ccType.replace(/'%20'/g, ' ')},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})

router.get('/CCTypeTransactionCount/count', function(req, res, next) {
    ccTransactionModel.count(function(err,author){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: author};
        res.send(count);
    });
})

router.get('/CCTransaction/:start/:range', function(req, res, next) {
    ccTransactionModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/CCTransactionById/:ccTransactionMongoId',function(req,res,next){
    ccTransactionModel.findOne({"_id":req.params.ccTransactionMongoId},function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

});







router.post('/editCCTransactionModel/:CCTransactionId', function(req, res, next) {
    ccTransactionModel.findOneAndUpdate({"_id":req.params.CCTransactionId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})


router.delete('/CCTransactionByiD/:CCTransactionMongoId',function(req, res, next){
    console.log('fuelMongoId', req.params.CCTransactionMongoId);
    ccTransactionModel.remove({"_id":req.params.CCTransactionMongoId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});




