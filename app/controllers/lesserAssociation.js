var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    lesserAssociationModel = mongoose.model('lesserAssociation');

module.exports = function (app){
    app.use('/', router);
};


router.post('/lesserAssociation', function(req, res, next) {

    var lesserDetailsModel = new lesserAssociationModel(req.body);
    lesserDetailsModel.save(function(err, result) {
        if (err){
            console.log('fuelDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/lesserAssociation/count', function(req, res, next) {
    lesserAssociationModel.count(function(err,author){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: author};
        res.send(count);
    });
})

router.get('/lesserAssociation/:start/:range', function(req, res, next) {
    lesserAssociationModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})


router.get('/lesserAssociation/:lesserAssociationMongoId',function(req,res,next){
    lesserAssociationModel.findOne({"_id":req.params.lesserAssociationMongoId},function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

});



router.post('/lesserAssociation/:lesserAssociationMongoId', function(req, res, next) {

    lesserAssociationModel.findOneAndUpdate({"_id":req.params.lesserAssociationMongoId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})

router.get('/LesserCCTypeDetails/:ccType', function(req, res, next) {

    lesserAssociationModel.find({"corporateContract" :req.params.ccType.replace(/'%20'/g, ' ')},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})

router.delete('/lesserAssociation/:lesserAssociationMongoId',function(req, res, next){

    lesserAssociationModel.remove({"_id":req.params.lesserAssociationMongoId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});

router.get('/vehicleContractByLeasedType/:lesserName/:ccType/:vehicleType/:companyName',function(req,res,next){

    let ccType=req.params.ccType.replace(/'%20'/g, ' ')
    let vehicleType=req.params.vehicleType.replace(/'%20'/g, ' ')
    let lesserName=req.params.lesserName.replace(/'%20'/g, ' ')
    let companyName=req.params.companyName.replace(/'%20'/g, ' ')
    console.log(ccType)
    console.log(vehicleType)
    console.log(lesserName)
    console.log(companyName)

    lesserAssociationModel.find({$and :
                [
                    {$and:
                           [
                    {"companyName":companyName} ,
                    {"lesserName":lesserName} ,
                    {"corporateContract": ccType},
                    {"vehicleType":vehicleType} ]}

                ]

        },function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                console.log(result);
                res.send(result);

            }
        })
});

