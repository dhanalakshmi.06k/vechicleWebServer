var express = require('express'),
    router = express.Router(),
    mongoose = require('mongoose'),
    VehicleCorporateContractModel = mongoose.model('vehicleCorporateContract');

module.exports = function (app){
    app.use('/', router);
};


router.post('/vehicleCorporateContract', function(req, res, next) {
    var corporateContractModel = new VehicleCorporateContractModel(req.body);
    corporateContractModel.save(function(err, result) {
        if (err){
            console.log('fuelDetailsPost failed: ' + err);
        }
        res.send(result);
    });
});

router.get('/vehicleCorporateContract/count', function(req, res, next) {
    VehicleCorporateContractModel.count(function(err,author){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: author};
        res.send(count);
    });
})

router.get('/vehicleCorporateContract/:start/:range', function(req, res, next) {
    VehicleCorporateContractModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range))

})

router.get('/allvehicleCorporateContractByContractType/:ccType', function(req, res, next) {
    VehicleCorporateContractModel.find({'corporateContract':req.params.ccType},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})


router.get('/vehicleCorporateContractByContractTypeByRange/:ccType/:start/:range', function(req, res, next) {
    VehicleCorporateContractModel.find({"corporateContract":req.params.ccType},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    }).skip(parseInt(req.params.start)).limit(parseInt(req.params.range));

})

router.get('/vehicleCorporateContractByContractCount/:ccType', function(req, res, next) {
    VehicleCorporateContractModel.count({"corporateContract":req.params.ccType},function(err,countData){
        if(err){
            res.status(500).send(err.message);
            logger.info(err.message);
        }
        var count = {count: countData};
        res.send(count);
    });
})


router.get('/allCorporateContract', function(req, res, next) {
    VehicleCorporateContractModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})



router.get('/allvehicleCorporateContractDropDown', function(req, res, next) {
    VehicleCorporateContractModel.find({},function(err,result){
        if(err){
            res.send(err)
            console.log(err.stack)
        }else{
            res.send(result)
        }

    })

})


router.get('/vehicleCorporateContractBymongoId/:vehicleCorporateContractMongoId',function(req,res,next){
    VehicleCorporateContractModel.find({"_id":req.params.vehicleCorporateContractMongoId},function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

});
router.get('/companyCorporateContractByMongodbId/:companyMongoDbID',function(req,res,next){


    VehicleCorporateContractModel.find({"companySelected._id":req.params.companyMongoDbID},function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

});
router.get('/companyCCByCompanyNameAndCCTypeAndVehicleTypeAndShiftType/:companyName/:corporateContract/:vehicleType/:shiftType',function(req,res,next){

    VehicleCorporateContractModel.find({
        $and :
            [
                {$and: [{"companySelected.companyName":req.params.companyName} , { "corporateContract" : req.params.corporateContract.replace(/'%20'/g, ' ') }]} ,
                {$and: [{"companySelected.companyName":req.params.companyName} , { "vehicleTypeSelected.vehicleTypeName" : req.params.vehicleType.replace(/'%20'/g, ' ')}]},
                {$and: [{"companySelected.companyName":req.params.companyName} , { "shiftTypeSelected.ShiftTypeName" : req.params.shiftType.replace(/'%20'/g, ' ')}]}]

    },function(err,result){
        if(err)
        {
            console.log(err);
            res.send(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

})

router.get('/companyCCByCompanyNameAndCCTypeAndVehicleType/:companyName/:corporateContract/:vehicleType',function(req,res,next){

    VehicleCorporateContractModel.find({
        $and :
            [
                {$and: [{"companySelected.companyName":req.params.companyName} , { "corporateContract" : req.params.corporateContract.replace(/'%20'/g, ' ') }]} ,
                {$and: [{"companySelected.companyName":req.params.companyName} , { "vehicleType" : req.params.vehicleType.replace(/'%20'/g, ' ')}]}]

    },function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

})

router.get('/companyCCByMongodbIdAndCCTypeAndVehicleType/:companyMongoDbID/:corporateContract/:vehicleType',function(req,res,next){

    VehicleCorporateContractModel.find({
        $and :
            [
                {$and: [{"companySelected._id":req.params.companyMongoDbID} , { "corporateContract" : req.params.corporateContract.replace(/'%20'/g, ' ') }]} ,
                {$and: [{"companySelected._id":req.params.companyMongoDbID} , { "vehicleType" : req.params.vehicleType.replace(/'%20'/g, ' ')}]}]

    },function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

})

router.get('/companyCCByMongodbIdAndCCTypeAndVehicleTypeAndShiftType/:companyMongoDbID/:corporateContract/:vehicleType/:shiftType',function(req,res,next){

    VehicleCorporateContractModel.find({
        $and :
            [
                {$and: [{"companySelected._id":req.params.companyMongoDbID} , { "corporateContract" : req.params.corporateContract.replace(/'%20'/g, ' ') }]} ,
                {$and: [{"companySelected._id":req.params.companyMongoDbID} , { "vehicleTypeSelected.vehicleTypeName" : req.params.vehicleType }]},
                {$and: [{"companySelected._id":req.params.companyMongoDbID} , { "shiftTypeSelected.ShiftTypeName" : req.params.shiftType }]}
            ]

    },function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

})


router.get('/companyCCByMongodbIdAndCCType/:companyMongoDbID/:corporateContract',function(req,res,next){

    VehicleCorporateContractModel.find({ $and :
            [ {"_id":req.params.companyMongoDbID}, { "corporateContract" : req.params.corporateContract.replace(/'%20'/g, ' ') } ] },function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

});

router.get('/comapnyCorporateContract/:CCType',function(req,res,next){

    VehicleCorporateContractModel.find({"corporateContract": req.params.CCType.replace(/'%20'/g, ' ')},function(err,result){
        if(err)
        {
            console.log(err);
        }
        else
        {
            console.log(result);
            res.send(result);

        }
    })

});


router.post('/editVehicleCorporateContract/:vehicleCorporateContractMongoId', function(req, res, next) {
    console.log("*******????????????11111111111111111?????")
    console.log(req.params.vehicleCorporateContractMongoId)
    VehicleCorporateContractModel.findOneAndUpdate({"_id":req.params.vehicleCorporateContractMongoId},req.body,{upsert: true, new: true},
        function(err,result)
        {
            if(err){
                console.log(err.stack)
            }
            else{
                res.send(result)
            }

        });

})


router.delete('/vehicleCorporateContractBymongoId/:vehicleCorporateContractMongoId',function(req, res, next){
    VehicleCorporateContractModel.remove({"_id":req.params.vehicleCorporateContractMongoId},function(err,result)
    {
        if(err)
        {
            console.log(err);
        }
        else
        {
            res.send(result)
        }

    });
});


router.get('/vehicleContractByOwnedType/:vehicleType/:ccType/:companyName', function(req, res, next) {
    let ccType=req.params.ccType.replace(/'%20'/g, ' ')
    let vehicleType=req.params.vehicleType.replace(/'%20'/g, ' ')
    let companyName=req.params.companyName.replace(/'%20'/g, ' ')

    console.log(companyName)
    console.log(vehicleType)
    console.log(ccType)

    VehicleCorporateContractModel.aggregate([
        {
            $match: {
                $and: [
                    {"companyName":companyName},
                    {"vehicleType":vehicleType},
                    {"corporateContract": ccType}
                ]
            }
        }
    ], function (err, result) {
        if (err) {
            console.log(err.stack)
            res.send(err)

        } else {
            res.send(result)
        }

    })
})









router.get('/getccBasedOnNameAndType/:companyName/:corporateContract/:vehicleTypeName',function(req,res,next){
    let ccType=req.params.corporateContract.replace(/'%20'/g, ' ')
    let vehicleType=req.params.vehicleTypeName.replace(/'%20'/g, ' ')
    let companyName=req.params.companyName.replace(/'%20'/g, ' ')
    VehicleCorporateContractModel.find({$and :
                [
                    {$and: [{"companySelected.companyName": companyName } , { "corporateContract" :ccType }]} ,
                    {$and: [{"vehicleType":vehicleType} ,{ "corporateContract" : ccType }]} ,

                ]

        }
        ,function(err,result){
            if(err)
            {
                console.log(err);
            }
            else
            {
                console.log(result);
                res.send(result);

            }
        })

})



