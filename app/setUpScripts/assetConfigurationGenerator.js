/**
 * Created by Suhas on 7/5/2016.
 */
let mongoose = require('mongoose'),
    config = require('../../config/config');
let assetParameterConfigModel = require('../models/assetConfiguration');

let db = mongoose.connection;
mongoose.connect(config.db,function(){
    console.log("MongoDb Connected On  "+config.db);
});
db.on('error', function (){
    throw new Error('unable to connect to database at ' +config.db);
});

let assetParameterConfiguration = [
    {
        "assetType" :{
            "typeId":"Vehicle",
            "label":"Vehicle",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },
        "configuration" : {
            "name" : {
                "type" : "text",
                "field":"name",
                "description" : "Enter Vehicle  Name",
                "label":"Name",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "registrationNumber" : {
                "type" : "number",
                "field":"registrationNumber",
                "description" : "Enter Registration Number",
                "label":"Registration Number",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },
            "chassisNumber" : {
                "type" : "number",
                "field":"chassisNumber",
                "description" : "Enter Chassis Number",
                "label":"Chassis Number",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },


            "tankCapacity" : {
                "type" : "number",
                "field":"tankCapacity",
                "description" : "Enter Tank Capacity",
                "label":"Tank Capacity",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"number"
            },
            "vehicleNo" : {
                "field":"vehicleNo",
                "type" : "text",
                "description" : "Enter Vehicle Number",
                "label":"Vehicle Number",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "tsNo" : {
                "field":"tsNo",
                "type" : "text",
                "description" : "Enter TS  Number",
                "label":"TS No",
                "required":true,
                "autoComplete":false,
                "assetName":true,
                "fieldDisplayType":"text"
            },
            "vehicleType" : {
                "field":"vehicleType",
                "type" : "text",
                "description" : "Enter Vehicle Type",
                "label":"VehicleType",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "registration Date" : {
                "field":"registrationDate",
                "type" : "date",
                "description" : "Enter Registration Date",
                "label":"Registration Date",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"date"
            },

            "dateofJoining" : {
                "field":"dateofJoining",
                "type" : "date",
                "description" : "Enter Date of Joining",
                "label":"Date of Joining",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"date"
            },
            "rentalType" : {
                "field":"rentalType",
                "type" : "dropDown",
                "description" : "Select Rental Type",
                "label":"Rental Type",
                "required":false,
                "autoComplete":true,
                "fieldDisplayType":"dropDown",
                "value":{
                    "dropDownValues": ["Own","Leased"],
                    "defaultValue":"Own"
                }
            }
        }
    },
    {
        "assetType" :{
            "typeId":"Lesser",
            "label":"Lesser",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:1
        },
        "configuration" : {
            "lesserName" : {
                "type" : "text",
                "field":"lesserName",
                "description" : "Enter Lesser  Name",
                "label":"Lesser Name",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "lesserAddress" : {
                "field":"lesserAddress",
                "type" : "text",
                "description" : "Enter Lesser Address",
                "label":"Lesser Address",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "dateofJoining" : {
                "field":"dateofJoining",
                "type" : "date",
                "description" : "Enter Date of Joining ",
                "label":"Date of Joining",
                "required":false,
                "autoComplete":false,
                "assetName":true,
                "fieldDisplayType":"date"
            }
        }
    },
    {
        "assetType" :{
            "typeId":"Company",
            "label":"Company",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:2
        },
        "configuration" : {
            "companyName" : {
                "type" : "text",
                "field":"companyName",
                "description" : "Enter Company Name",
                "label":"Company Name",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },

            "companyLocality" : {
                "type" : "text",
                "field":"companyLocality",
                "description" : "Enter Company Locality",
                "label":"Company Locality",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "companyRegion" : {
                "type" : "text",
                "field":"companyRegion",
                "description" : "Enter Company Region",
                "label":"Company Region",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "companyPostcode" : {
                "type" : "text",
                "field":"companyPostcode",
                "description" : "Enter Postcode",
                "label":"Postcode",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "country" : {
                "type" : "text",
                "field":"country",
                "description" : "Enter country",
                "label":"Country",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },

            "officeNumber" : {
                "field":"officeNumber",
                "type" : "text",
                "description" : "Enter office Number",
                "label":" office Number",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
        /*    "corporate contract" : {
                "field":"corporateContract",
                "type" : "dropDown",
                "description" : "Select corporate Contract",
                "label":"corporate Contract",
                "required":false,
                "autoComplete":true,
                "fieldDisplayType":"dropDown",
                "value":{
                    "dropDownValues": ["contract type 1","contract type  2","contract type  3","contract type 4","contract type  5"],
                    "defaultValue":"Male"
                }
            },*/
            "personOfContact" : {
                "field":"personOfContact",
                "type" : "text",
                "description" : "Enter person Of Contact",
                "label":" Person Of Contact",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "CompanyEmailID" : {
                "field":"CompanyEmailID",
                "type" : "text",
                "description" : "Enter Company Email ID",
                "label":" Company Email ID",
                "required":true,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },

            "status" : {
                "field":"status",
                "type" : "dropDown",
                "description" : "Select Status",
                "label":"Status",
                "required":false,
                "autoComplete":true,
                "fieldDisplayType":"dropDown",
                "value":{
                    "dropDownValues": ["active","inactive"],
                    "defaultValue":"active"
                }
            },
        "completeCompanyAddress" : {
            "field":"completeCompanyAddress",
            "type" : "text",
            "description" : "Enter Company Address",
            "label":" Complete Company Address",
            "required":false,
            "autoComplete":false,
            "fieldDisplayType":"textarea"
        },



    }
    },
    {
        "assetType" :{
            "typeId":"Driver",
            "label":"Driver",
            theme:{
                color:{
                    primary:"#5C6BC0",
                    secondary:"#7986CB"
                }
            },
            order:4
        },
        "configuration" : {
            "driverName" : {
                "type" : "text",
                "field":"driverName",
                "description" : "Enter Driver Name",
                "label":"Driver Name",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "licenseNumber" : {
                "field":"licenseNumber",
                "type" : "text",
                "description" : "Enter License Number",
                "label":"License Number",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },

            "status" : {
                "field":"status",
                "type" : "dropDown",
                "description" : "Select Status",
                "label":"Status",
                "required":false,
            "fieldDisplayType":"dropDown",
                "autoComplete":true,
                "value":{
                    "dropDownValues": ["active","inactive"],
                    "defaultValue":"active"
                }
            },
            "street" : {
                "field":"street",
                "type" : "text",
                "description" : "Enter street",
                "label":"Street",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "city" : {
                "field":"city",
                "type" : "text",
                "description" : "Enter city",
                "label":"City",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "state" : {
                "field":"state",
                "type" : "text",
                "description" : "Enter state",
                "label":"State",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "postalCode" : {
                "field":"postalCode",
                "type" : "text",
                "description" : "Enter postalCode",
                "label":"Postal Code",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "country" : {
                "field":"country",
                "type" : "text",
                "description" : "Enter country",
                "label":"country",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },

            "licenseValidity" : {
                "field":"licenseValidity",
                "type" : "date",
                "description" : "Enter License Validity",
                "label":"License Validity",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"date"
            },
            "emailId" : {
                "field":"emailId",
                "type" : "text",
                "description" : "Enter emailId",
                "label":"EmailId",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },
            "phNumber" : {
                "field":"phNumber",
                "type" : "text",
                "description" : "Enter phNumber",
                "label":"phone Number",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"text"
            },

            "comments" : {
                "field":"comments",
                "type" : "text",
                "description" : "Enter Comments",
                "label":"Comments",
                "required":false,
                "autoComplete":false,
                "fieldDisplayType":"textarea"
            }

        }
    },
];
function generateSensorConfiguration(){
    assetParameterConfigModel.insertMany(assetParameterConfiguration,function(err,res){
        console.log("Added "+res.length+" Asset Configuration");
        stopExecution();
    })

}
function stopExecution(){
    process.exit(0);
}
function removePreviousData(callBack){
    assetParameterConfigModel.remove({},function(err,result){
        console.log("Deleting previous Asset Config Data");
        generateSensorConfiguration(callBack);
    })
}
removePreviousData(function(){
    console.log("Started generating Data")
});
module.exports ={
    run:removePreviousData
};

