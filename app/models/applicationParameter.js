/**
 * Created by Suhas on 7/5/2016.
 */
let mongoose = require('mongoose'),
    Schema = mongoose.Schema;
let applicationParameterConfigurationSchema =  new Schema({
    updated: { type: Date, default: Date.now },
    created: { type: Date}},{strict:false},{collection: "applicationParameterConfig"});
let applicationParameterConfig = mongoose.model('applicationParameterConfig',applicationParameterConfigurationSchema);
applicationParameterConfigurationSchema.pre('save', function (next) {
    this.created = new Date();
    next();
});
module.exports = applicationParameterConfig;