var mongoose = require('mongoose'),
  Schema = mongoose.Schema;
var ElectionResultSchema = new mongoose.Schema(
{
       rateField:String,
       km:Number,
       extraKm:Number,
       extraHour:Number,
       outStation:String,
       driverBeta:Number

},
{collection:"shiftManagementDetails"});
mongoose.model('shiftManagementDetails',ElectionResultSchema);